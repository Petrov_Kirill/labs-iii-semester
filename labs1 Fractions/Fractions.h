#ifndef _FRACTIONS_H

#define _FRACTIONS_H

#include <iostream>

using namespace std;

class Fraction
{
public:
	Fraction ();
	Fraction (int, int);
	Fraction (const Fraction &);
	Fraction operator+ (const Fraction &) const;
	Fraction operator- (const Fraction &) const;
	Fraction operator* (const Fraction &) const;
	Fraction operator/ (const Fraction &) const;
	Fraction operator+ (int) const;
	Fraction operator- (int) const;
	Fraction operator* (int) const;
	Fraction operator/ (int) const;
	Fraction & operator+= (const Fraction &);
	Fraction & operator-= (const Fraction &);
	Fraction & operator= (const Fraction &);
	void Adduction ();
	friend ostream & operator<< (ostream &, Fraction &);
	friend istream & operator>> (istream &, Fraction &);
private:
	int numenator;
	int denominator;
};

#endif