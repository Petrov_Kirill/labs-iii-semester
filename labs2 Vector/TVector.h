#ifndef _TVECTOR_H
#define _TVECTOR_H

#include "exclusion.h"
#include <iostream>
#include <locale.h>
#include <time.h>

using namespace std;

template <typename TypeElem>
class TVector
{
private:
	TypeElem *data;
	unsigned int size;
public:
	TVector ();
	TVector (unsigned int);
	TVector (const TVector &);
	~TVector ();
	TVector operator+ (const TVector &) const;
	TVector operator+ (const TypeElem &) const;
	TVector operator- (const TVector &) const;
	TVector operator- (const TypeElem &) const;
	TVector & operator= (const TVector &);
	TVector & operator+= (const TVector &);
	TVector & operator+= (const TypeElem &);
	TVector & operator-= (const TVector &);
	TVector & operator-= (const TypeElem &);
	bool operator== (const TVector &) const;
	void RandomFill ();
	void Zeroing ();
 	template<typename TypeElem>
	friend ostream & operator<< (ostream & os, const TVector<TypeElem> & vect);
};

//����������� ��� ����������
template <typename TypeElem> TVector<TypeElem> :: TVector ()
{
	size=1;
	data=new TypeElem [size];
	data[0]=0;
}
//����������� � ����������
template <typename TypeElem> TVector<TypeElem> :: TVector (unsigned int _size)
{
	size=_size;
	data= new TypeElem [size];
	Zeroing();
}
//����������� �����������
template <typename TypeElem> TVector<TypeElem> :: TVector (const TVector<TypeElem> & vect)
{
	size=vect.size;
	data=new TypeElem [size];
	for (unsigned int i=0; i<size; i++)
		data[i]=vect.data[i];
}
//����������
template <typename TypeElem> TVector<TypeElem> :: ~TVector ()
{
	delete []data;
}
//�=�+�
template <typename TypeElem> TVector<TypeElem> TVector<TypeElem> :: operator+ (const TVector & vect) const
{
	try
	{
		if (size!=vect.size) throw SizeDifference();
		TVector result (size);
		for (unsigned int i=0; i<size; i++)
			result.data[i]=data[i]+vect.data[i];
		return result;
	}
	catch (SizeDifference er)
	{
		cout<<er.what();
		cout<<endl;
		TVector result;
		return result;
	}
}
//�=�+b
template <typename TypeElem> TVector<TypeElem> TVector<TypeElem> :: operator+ (const TypeElem & var) const
{
	try
	{
		if (size!=1) throw SizeDifference();
		TVector result (size);
		result.data[0]=data[0]+var;
	}
	catch (SizeDifference er)
	{
		cout<<er.what();
		cout<<endl;
		TVector result;
		return result;
	}
}
//�=�-�
template <typename TypeElem> TVector<TypeElem> TVector<TypeElem> :: operator- (const TVector & vect) const
{
	try
	{
		if (size!=vect.size) throw SizeDifference();
		TVector result (size);
		for (unsigned int i=0; i<size; i++)
			result.data[i]=data[i]-vect.data[i];
		return result;
	}
	catch (SizeDifference er)
	{
		cout<<er.what();
		cout<<endl;
		TVector result;
		return result;
	}
}
//�=�-b
template <typename TypeElem> TVector<TypeElem> TVector<TypeElem> :: operator- (const TypeElem & var) const
{
	try
	{
		if (size!=1) throw SizeDifference();
		TVector result (size);
		result.data[0]=data[0]+var;
	}
	catch (SizeDifference er)
	{
		cout<<er.what();
		cout<<endl;
		TVector result;
		return result;
	}
}
//A=B
template <typename TypeElem> TVector<TypeElem> & TVector<TypeElem> :: operator= (const TVector<TypeElem> & vect)
{
	try
	{
		if (this==&vect) throw Indicates();
		if (vect.data==NULL) throw BasedatZero();
		delete[] data;
		data=new TypeElem [size=vect.size];
		for (unsigned i=0; i<size; i++)
			data[i]=vect.data[i];
	}
	catch (Indicates)
	{
	}
	catch (BasedatZero)
	{
		delete[] data;
		size=1;
		data=new TypeElem [size];
		data[0]=0;
	}
	return *this;
}
//A+=B
template <typename TypeElem> TVector<TypeElem> & TVector<TypeElem> :: operator+= (const TVector<TypeElem> & vect)
{
	try
	{
		if (size!=vect.size) throw SizeDifference();
		for (unsigned int i=0; i<size; i++)
			data[i]+=vect.data[i];
	}
	catch (SizeDifference er)
	{
		cout<< er.what();
		cout<< endl;
		delete[] data;
		size=1;
		data=new TypeElem [size];
		data[0]=0;
	}
	return *this;
}
//A+=b
template <typename TypeElem> TVector<TypeElem> & TVector<TypeElem> :: operator+= (const TypeElem & var)
{
	try
	{
		if (size!=1) throw SizeDifference();
		data[0]+=var;
	}
	catch (SizeDifference er)
	{
		cout<< er.what();
		cout<< endl;
		delete[] data;
		size=1;
		data=new TypeElem [size];
		data[0]=0;
	}
	return *this;
}
//A-=B
template <typename TypeElem> TVector<TypeElem> & TVector<TypeElem> :: operator-= (const TVector<TypeElem> & vect)
{
	try
	{
		if (size!=vect.size) throw SizeDifference();
		for (unsigned int i=0; i<size; i++)
			data[i]-=vect.data[i];
	}
	catch (SizeDifference er)
	{
		cout<< er.what();
		cout<< endl;
		delete[] data;
		size=1;
		data=new TypeElem [size];
		data[0]=0;
	}
	return *this;
}
//A-=b
template <typename TypeElem> TVector<TypeElem> & TVector<TypeElem> :: operator-= (const TypeElem & var)
{
	try
	{
		if (size!=1) throw SizeDifference();
		data[0]-=var;
	}
	catch (SizeDifference er)
	{
		cout<< er.what();
		cout<< endl;
		delete[] data;
		size=1;
		data=new TypeElem [size];
		data[0]=0;
	}
	return *this;
}
//A==B
template <typename TypeElem> bool TVector<TypeElem> :: operator== (const TVector<TypeElem> &vect) const
{
	if (size!=vect.size)
		return false;
	else
	{
		for (unsigned int i=0; i<size; i++)
				if (data[i]!=vect.data[i])
					return false;
	}
	return true;
}
//������������� ��������� ���������
template <typename TypeElem> void TVector<TypeElem> :: RandomFill ()
{
	srand(time(0));
	for (unsigned int i=0; i<size; i++)
		data[i]=rand()%10;
}
//������������� ������� ���������
template <typename TypeElem> void TVector<TypeElem> :: Zeroing ()
{
	for (unsigned int i=0; i<size; i++)
		data[i]=0;
}
//����� � �����
template <typename TypeElem> ostream & operator<< (ostream & os, const TVector <TypeElem> & vect)
{
	for (unsigned int i=0; i<vect.size; i++)
		os << vect.data[i]<<" ";
	os << endl;
	return os;
}

#endif